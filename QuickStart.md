# Quick Start 
The utility takes one argument, which is a path to the parameters file.

##Building

The main method of the application is at the following path:
**./src/main/java/com/reltio/rocs/PeriodicTask/​ ​ExecutePeriodicTask.java**

##Dependencies 

1. reltio-cst-core-1.4.9
2. gson-2.2.4​

##Parameters File Example

```
#!paintext

#Common Properties
TENANT_ID=swyQZ6vj3SlyfaM
ENVIRONMENT_URL=sndbx.reltio.com
USERNAME=username@reltio.com
PASSWORD=*****
CLIENT_CREDENTIALS=
AUTH_URL=https://auth.reltio.com/oauth/token
HTTP_PROXY_HOST=
HTTP_PROXY_PORT=



#Tool Specific Properties
API_HISTORY=https://sndbx.reltio.com/reltio/tasks/
BODY=/Users/yiranliu/workspace/body.txt
REPORT_INTERVAL=60

```
##Executing

Command to start the utility.
```
#!plaintext

java -jar reltio-util-periodicTask-{{version}}-jar-with-dependencies.jar “configuration.properties” > $logfilepath$

Please use the latest version from the bitbucket.
```
