package com.reltio.rocs.periodicTask;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;

public class ExecutePeriodicTask {
	private static Gson gson = new Gson();
	private static final Logger LOGGER = LogManager.getLogger(ExecutePeriodicTask.class.getName());
	private static final Logger logPerformance = LogManager.getLogger("performance-log");

	public static void main(String[] args) throws Exception {

		LOGGER.info(new Date() + " [Info]Process started..");
		Properties properties = new Properties();
		String propertyFilePath=null;
		try {
			propertyFilePath = args[0];
			properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

			Util.setHttpProxy(properties);
		} catch (Exception e) {
			LOGGER.error("Failed reading properties File...", e);
			e.printStackTrace();
		}
		// READ the Properties values
		final String tenant_id = properties.getProperty("TENANT_ID");
		final String environment_url = properties.getProperty("ENVIRONMENT_URL");
		final String apiHistory = properties.getProperty("API_HISTORY");
		final String authUrl = properties.getProperty("AUTH_URL");
		final String username = properties.getProperty("USERNAME");
		
		
		final String password = properties.getProperty("PASSWORD");
		
		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		
		List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("API_HISTORY", "BODY", "AUTH_URL", "REPORT_INTERVAL", "TENANT_ID","ENVIRONMENT_URL"));

        if (!missingKeys.isEmpty()) {
        	LOGGER.info("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }
		
		
		final String body = new String(Files.readAllBytes(Paths.get(properties.getProperty("BODY"))));
		Integer interval = 60;
		
		final String tenant_url = "https://" + environment_url + "/reltio/api/"+ tenant_id+"/"+"esCassandraConsistencyCheck/";

		try {
			if (isInteger(properties.getProperty("REPORT_INTERVAL"))) {
				Integer temp = Integer.parseInt(properties.getProperty("REPORT_INTERVAL"));
				interval = temp > 5 ? temp : 5;
			}
		} catch (NullPointerException e) {
			LOGGER.error("Missing property: REPORT_INTERVAL. REPORT_INTERVAL is set to 60 seconds.", e);
		}

		ReltioAPIService reltioAPIService = Util.getReltioService(properties);
		String response = "";

		try {
			response = reltioAPIService.post(tenant_url, body);
			LOGGER.info(response + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Response[] jResponse = gson.fromJson(response, new TypeToken<Response[]>() {
		}.getType());

		boolean eol = false;
		Response getRespnse;

		while (!eol) {

			for (Response res : jResponse) {

				String getResponse = reltioAPIService.get(apiHistory + res.getId());
				getRespnse = gson.fromJson(getResponse, new TypeToken<Response>() {
				}.getType());

				if (getRespnse.status.equalsIgnoreCase("COMPLETED") && getRespnse.id.equals(res.getId())) {
					LOGGER.debug(getResponse + "\n"+"Task ID: " + res.getId()+
							"\nTotal Time taken for the process to complete: " + getRespnse.duration);
					eol = true;
					break;
				}
				if (!eol) {
					try {
						Thread.sleep(interval * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}
		LOGGER.info(new Date() + " [Info]Process ended..");
	}

	public static boolean isInteger(String str) {
		str = str.trim();
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}
}
